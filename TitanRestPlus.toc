## Interface: 80000
## Title: Titan Panel [|cffeda55fRest+|r] |cff00aa008.0.0.1|r
## Notes: Keeps track of the RestXP amounts and status for all of your characters. Is it updated to run on patch 8.x.
## Author: Current - yonohu/Kernigha, Vogonjeltz and Madysen
## Author: GrayElf, Maillen, Vogonjeltz, Madysen, yonohu/Kernighan
## DefaultState: Enabled
## SavedVariables: RestPlus_Data, RestPlus_Settings, RestPlus_Colors
## OptionalDeps:
## Dependencies: Titan
## Version: 8.0.1.0
## X-Category: Interface Enhancements
## X-Date: 2018-08-23
## X-Child-of: Titan
## X-Curse-Packaged-Version: 8.0.1.0
## X-Curse-Project-Name: Titan Panel [RestPlus]
## X-Curse-Project-ID: rest_plus
## X-Curse-Repository-ID: wow/rest_plus/mainline
TitanRestPlus.xml
